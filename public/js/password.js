window.addEventListener('DOMContentLoaded', function(){

  // パスワード入力欄とボタンのHTMLを取得
  let btn_passview = document.getElementById("btn_passview");
  let input_pass = document.getElementById("input_pass");
  let input_pass2 = document.getElementById("input_pass2");
  // ボタンのイベントリスナーを設定
  btn_passview.addEventListener("click", (e)=>{

    // パスワード入力欄のtype属性を確認
    if( input_pass.type === 'password' ) {
      // パスワードを表示する
      input_pass.type = 'text';
    } else {
      // パスワードを非表示にする
      input_pass.type = 'password';
    }
	  
    // パスワード入力欄のtype属性を確認
    if( input_pass2.type === 'password' ) {
      // パスワードを表示する
      input_pass2.type = 'text';
    } else {
      // パスワードを非表示にする
      input_pass2.type = 'password';
    }
	  
  });

});